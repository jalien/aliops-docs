# Recovery Procedures

Here we have procedures that may be helpful.

## Software RAID: OS Setup

For storage configuration during OS installation the following steps allow to setup software RAID1 (mirror):

1. Ensure all devices to be used for RAID are bootable. This option creates a 1MB _bios\_grub_ partition.
2. Add an __unformatted partition__ for each of the disks. Make sure it is of equal size.
3. __Create software RAID (md)__ using the above partitions.
4. Add _/boot_ and _/_ parititions on the new md device. Example configuration is as follows:

![RAIDSetup](RAID_OSsetup.png)

## [WIP] Software RAID: Replacing Disk

Instructions in progress of writing based on [this](https://blog.tinned-software.net/replace-hard-disk-from-software-raid/).
