# RecoveryVer2 Tool

For file batch operations a command line tool was developed to help with the most database-intensive operations. Useful terms:

- **Storage element**: When specifying storage element names, use the precise name as listed [here](http://alimonitor.cern.ch?632).
- **Transfer ID**: This parameter refers to the transfer job ID as listed [here](http://alimonitor.cern.ch/transfers).
- **File list**: For certain operations it is expected to provide a plain text file with lists of GUIDs/LFNs that you want to operate on.


## archive register

Register an archive and its members within the Catalogue. Command line options are as follows:

| Required | Option | Parameter _(Default)_ | Description |
|:--------:|:-------|:----------------------|:------------|
| &#9745;  | -list, -l | <a href="#">File list</a>  | List of archive LFNs |
|          | -user  | Username _(aliprod)_ | Register archive with specific user role |

!!! example
    ```console
    ~$ ./run.sh utils.RecoveryVer2 archive register ...
    ```

## clean orphaned

This command deletes LFNs from the catalogue by GUIDs listed in the file. Command line options are as follows:

| Required | Option | Parameter _(Default)_ | Description |
|:--------:|:-------|:----------------------|:------------|
| &#9745;  | -list, -l | <a href="#">File list</a> | List of orphaned GUIDs |


!!! example
    ```
    ~$ ./run.sh utils.RecoveryVer2 clean orphaned -l files.list
    ```

## hyperloop

### add

Add a new transfer/removal request. Function: ```HyDataOperations.add(boolean pin, int trainid, String targetse, String period)```

| Required | Option | Parameter _(Default)_ | Description |
|:--------:|:-------|:----------------------|:------------|
| &#9745;  | -pin, -p | true/false | Data to be replicated (true) or removed (false) |
| &#9745;  | -trainid, -t | <a href="#">Train ID</a> | Train ID of the data set |
| &#9745;  | -se, -s | <a href="#">ALICE::NAME::SE</a> | Target SE where to copy/remove the data |
| &#9745;  | -period, -p | <a href="#">Period name</a> | Period name |

!!! example
    ```
    ~$ ./run.sh utils.RecoveryVer2 hyperloop add -p true -t 148 -se 'ALICE::GSI::SE2' -p 'LHC15e_Low'
    ```
    
### stats
    
Get status for a specific transfer request. Function: ```HyDataOperations.stats(boolean pin, int trainid, String targetse, String period)```

| Required | Option | Parameter _(Default)_ | Description |
|:--------:|:-------|:----------------------|:------------|
| &#9745;  | -pin, -p | true/false | Data to be replicated (true) or removed (false) |
| &#9745;  | -trainid, -t | <a href="#">Train ID</a> | Train ID of the data set |
| &#9745;  | -se, -s | <a href="#">ALICE::NAME::SE</a> | Target SE where to copy/remove the data |
| &#9745;  | -period, -p | <a href="#">Period name</a> | Period name |

!!! example
    ```
    ~$ ./run.sh utils.RecoveryVer2 hyperloop stats -p true -t 148 -se 'ALICE::GSI::SE2' -p 'LHC15e_Low'
    ```

### check

Manage pending requests. Function: ```HyDataOperations.check()```

!!! example
    ```
    ~$ ./run.sh utils.RecoveryVer2 hyperloop check
    ```


## info 

### analyze-type

Check how many (lost/transferred/orphaned/dark) files are of each type (root, log, other). Command line options are as follows:

| Required | Option | Parameter _(Default)_ | Description |
|:--------:|:-------|:----------------------|:------------|
| &#9745; | -list | <a href="#">File list</a> | List of GUIDs to check |

!!! example
    ```
    ~$ ./run.sh utils.RecoveryVer2 info analyze-type -list file.list
    ```

### check-lfns

Checks all replicas for a transfer job LFNs by downloading. Command line options are as follows:

| Required | Option | Parameter _(Default)_ | Description |
|:--------:|:-------|:----------------------|:------------|
|| -joblist | <a href="#">File list</a> | Get LFNs from list of transfer jobs |
|| -jobid | <a href="#">transferID</a> | Specific Job ID to check |
|| -lfn | File path | LFN to check |
|| -list | <a href="#">File list</a> | List of LFNs to check |
|| -se | <a href="#">ALICE::NAME::SE, ...</a> | List of storages to be excluded from being checked |

!!! example
    ```
    ~$ ./run.sh utils.RecoveryVer2 info check-lfns ...
    ```

### dark

Read list of GUIDs from file. Check if GUID exists in the DB and matches the size (if provided in the file list). Command line options are as follows:

| Required | Option | Parameter _(Default)_ | Description |
|:--------:|:-------|:----------------------|:------------|
| &#9745; | -list, -l | <a href="#">File list</a> | List of LFNs |
|| -rawdata | _(false)_ | Set when recovery Raw data |
|| -skip_first_N_lines | _(0)_ | Set if you already processed N lines and want to continue with next |
|| -disableSizeCheck | _(true)_ | Set if you don't want to get GUIDs size from the file list |

!!! example
    ```
    ~$ ./run.sh utils.RecoveryVer2 info dark ...
    ```

### guid/lfn

Retrieves GUID and LFN details for a given file.

!!! example
    ```
    ~$ ./run.sh utils.RecoveryVer2 info guid 0a1b2c3d-4e5f6g...

    ~$ ./run.sh utils.RecoveryVer2 info lfn /alice/data/..
    ```

### guids2lfns

Get the list of GUIDs and produce the list of corresponding LFNs. Command line options are as follows:

| Required | Option | Parameter _(Default)_ | Description |
|:--------:|:-------|:----------------------|:------------|
| &#9745;  | -list, -l | <a href="#">File list</a> | List of GUIDs |

!!! example
    ```
    ~$ ./run.sh utils.RecoveryVer2 info guids2lfns ...
    ```

### lfns2guids

Get the list of LFNs and produce the list of corresponding GUIDs. Command line options are as follows:

| Required | Option | Parameter _(Default)_ | Description |
|:--------:|:-------|:----------------------|:------------|
| &#9745; | -list, -l | <a href="#">File list</a> | List of LFNs |
|| -rawdata | _(false)_ | Set when recovery Raw data |

!!! example 
    ```
    ~$ ./run.sh utils.RecoveryVer2 info lfns2guids ...
    ```

### recoverable

Sort files to see whether they can be recovered or not. This method looks for orphaned files, or in case of excludeSE set to some SE - single replica files, that are present only on this storage. Command line options are as follows:

| Required | Option | Parameter _(Default)_ | Description |
|:--------:|:-------|:----------------------|:------------|
| &#9745; | -list | <a href="#">File list</a> | List of GUIDs or PFNs |
| &#9745; | -se | <a href="#">ALICE::NAME::SE, ...</a> | Storages to exclude (specify at least one which has files from list) | 
|| -rawdata | _(false)_ | Set when recovering Raw data |

!!! example
    ```
    ~$ ./run.sh utils.RecoveryVer2 info recoverable -list file.list -se 'ALICE::CNAF::SE'
    ```

### unique

Sort files to see whether they can be recovered or not. This method looks for orphaned files, or in case of excludeSE set to some SE - single replica files, that are present only on this storage. Command line options are as follows:

| Required | Option | Parameter _(Default)_ | Description |
|:--------:|:-------|:----------------------|:------------|
| &#9745; | -list | <a href="#">File list</a> | List of GUIDs or PFNs |
| &#9745; | -se | <a href="#">ALICE::NAME::SE, ...</a> | Storages to exclude (specify at least one which has files from list) | 
|| -rawdata | _(false)_ | Set when recovering Raw data |

!!! example
    ```
    ~$ ./run.sh utils.RecoveryVer2 info unique -list file.list -se 'ALICE::CNAF::SE'
    ```

## recover 

### damaged-files

Delete damaged files in SE and mirror them back (or to another location). Command line options are as follows:

| Required | Option | Parameter _(Default)_ | Description |
|:--------:|:-------|:----------------------|:------------|
| &#9745; | -list, -l | <a href="#">File list</a> | List of damaged files |
| &#9745; | -se, -s | <a href="#">ALICE::NAME::SE</a> | SE with the damaged files |
|| -ignoreNoReplica | _(true)_ | Set to true if you want to copy files back to the damaged SE |
|| -target, -t | <a href="#">ALICE::NAME::SE</a> | Set target SE instead of recovering all files to source SE |
|| -fallback, -f | <a href="#">ALICE::NAME::SE</a> | Copy files to fallback SE if target SE fails or there a replica is already there |
|| -purge | _(false)_ | Physically delete files from source SE |
|| -comment | 'Transfer title' | Name to be passed to recovery transfer |
|| -cleanOrphaned | _(true)_ | Set to false, if you don't want remove orphaned files |
|| -rawdata | _(false)_ | Set to true if recovering Raw data |

!!! example
    ```
    ~$ ./run.sh utils.RecoveryVer2 recover damaged-files ...
    ```

### register-pfns

Register file replicas at SE (e.g. if they were previously removed by mistake). Command line options are as follows:

| Required | Option | Parameter _(Default)_ | Description |
|:--------:|:-------|:----------------------|:------------|
| &#9745; | -list, -l | <a href="#">File list</a> | List of GUIDs to be added to the storage element |
| &#9745; | -se, -s |  <a href="#">ALICE::NAME::SE</a> | Name of the storage element |
|| -commit | | Commit changes |
|| -recover-orphaned | _(true)_ | Also register orphaned GUIDs that were cleaned from the Catalogue |

!!! example
    ```
    ~$ ./run.sh utils.RecoveryVer2 recover register-pfns ...
    ```

## se 

### cleanup

[WIP] This is Work in Progress function that allows to move data to other SEs.
The following diagram outlines the dependent functions as well as implemented logic:
![se cleanup](se_cleanup.svg)

Command line options are as follows:

| Required | Option | Parameter _(Default)_ | Description |
|:--------:|:-------|:----------------------|:------------|
| &#9745;  | -config, -c | Configuration file path | Configuration csv file: <site name\>,<size in TB\>,<generic transfer flag\> |

!!! example
    ```
    ~$ ./run.sh utils.RecoveryVer2 se cleanup -c config.txt
    ```

### erase

Decommission SE and wipe all information about it from the database.

!!! example
    ```
    ~$ ./run.sh utils.RecoveryVer2 se erase _SE1,SE2,..._
    ```

### list-guids

Returns a list of GUIDs based on total and minimum size of files. Command line options are as follows:

| Required | Option | Parameter _(Default)_ | Description |
|:--------:|:-------|:----------------------|:------------|
| &#9745;  | -se, -s | <a href="#">ALICE::NAME::SE</a> | Source storage element |
| | -total, -t | <total size in GB\> _Default: 1 (GB)_ | Total amount of files to query for |
| | -min-size, -m | <min file size in B\> _Default: "10000000" (10 MB)_ | Minimum file size |
| | -offset, -o | <index gtable offset\> _Default: 0_ | GUID index offset |

### move

Move all files provided from one SE to another with a fallback option. Command line options are as follows:

| Required | Option | Parameter _(Default)_ | Description |
|:--------:|:-------|:----------------------|:------------|
| &#9745;  | -target, -t | <a href="#">ALICE::NAME::SE</a> | Target storage element |
| &#9745;  | -fallback, -f | <a href="#">ALICE::NAME::SE</a> | Fallback storage element if files already exist on target SE |
| &#9745;  | -se, -s | <a href="#">ALICE::NAME::SE</a> | Source storage element |
|| -list, -l | <a href="#">File list</a> | List of files from source storage. If no list is provided, whole SE will be copied |
|| -id1 | <a href="#">transferID</a> | Transfer ID if you want to add files to existing transfer |
|| -id2 | <a href="#">transferID</a> | Transfer ID for fallback SE if you want to add files to existing transfer |
|| -replicas | Number | Maximum number of replicas of the file on the Grid. If exceeded, the file would not be copied. |
|| -comment | 'Transfer title' | Title to be used for recovery transfer |
|| -exse | <a href="#">ALICE::NAME::SE,...</a> | Storage elements to exclude | 

!!! example
    To move all files from one storage to another use the provided ```-se``` and ```-target``` options. For files that are already present you will need to give a fallback SE with ```-fallback``` option. To ensure that files are scheduled to move make sure that the ```-replicas``` option is correctly set.

    N.B.: this approach attempts to asynchroniously move the file and PFN reference in the Catalogue.
    ```
    ~$ ./run.sh utils.RecoveryVer2 se move -t 'ALICE::IPNL::EOS' -s 'ALICE::IPNL::SE' -f 'ALICE::Clermont::SE' -replicas 3 -comment 'Copying IPNL::SE to IPNL::EOS'
    ```

### replicate

Replicate all files provided from one SE to another with a fallback option. Command line options are as follows:

| Required | Option | Parameter _(Default)_ | Description |
|:--------:|:-------|:----------------------|:------------|
| &#9745;  | -target, -t | <a href="#">ALICE::NAME::SE</a> | Target storage element |
| &#9745;  | -fallback, -f | <a href="#">ALICE::NAME::SE</a> | Fallback storage if files already exist on target SE |
|| -se, -s | <a href="#">ALICE::NAME::SE</a> | Source storage element |
|| -list, -l | <a href="#">File list</a> | List of files from source storage. If no list is provided, whole SE will be copied |
|| -id1 | <a href="#">transferID</a> | Transfer ID if you want to add files to existing transfer |
|| -id2 | <a href="#">transferID</a> | Transfer ID for fallback SE if you want to add files to existing transfer |
|| -replicas | Number | Maximum number of replicas of the file on the Grid. If exceeded, the file would not be copied. |
|| -move | _(false)_ | Move instead of copy. This will physically delete files from source SE |
|| -comment | 'Transfer title' | Title to be used for recovery transfer |
|| -exse | <a href="#">ALICE::NAME::SE,...</a> | Storages to exclude | 

!!! example
    To move all files from one storage to another use the provided ```-se``` and ```-target``` options. For files that are already present you will need to give a fallback SE with ```-fallback``` option. Make sure to set the ```-replicas``` option so that files get to be scheduled for replication.

    ```
    ~$ ./runs.sh utils.RecoveryVer2 se replicate -t 'ALICE::Vienna::EOS' -s 'ALICE::ICM::EOS' -l ICM.list -replicas 2 -comment 'Replicate ICM unique to Vienna' -f 'ALICE::Prague::SE'
    ```

### sync

Synchronize SE and the catalogue:
- Recover lost files on the storage
- Find the dark data in the Catalogue
- Compare and fix file sizes

Command line options are as follows:

| Required | Option | Parameter _(Default)_ | Description |
|:--------:|:-------|:----------------------|:------------|
|| -commit | | Commit changes, otherwise dry-run mode is applied |
|| -list, l | <a href="#">File list</a> | List of files from source storage |
|| -se, -s  | <a href="#">ALICE::NAME::SE</a> | Source element to be synchronized |
|| -date | 'YYYY MM DD HH:mm:ss' | Files newer than this time threshold will be ignored |
|| -disableSizeCheck | _(false)_ | Set if you don't want to get GUIDs size from file list |

!!! example
    ```
    ~$ ./run.sh utils.RecoveryVer2 se sync ...
    ```

## transfers 

### kill

Stop transfers by their transferIDs.

!!! example
    ```
    ~$ ./run.sh utils.RecoveryVer2 transfers kill _transferID_
    ```

### list-guids

Get files with __Error__ or __Pending__ statuses from transferID printed to the file failed.transfers.transferid. Command line options are as follows:

| Required | Option | Parameter _(Default)_ | Description |
|:--------:|:-------|:----------------------|:------------|
| &#9745; | -id | <a href="#">transferID</a> | | Transfer to look up |
|| -failed, -pending, -done, -running | | List files with a specific transfer state |
|| -pfn , -lfn | | Print PFN/LFN information |
|| -md5 | | Print MD5sum information |
|| -se  | | Print SE info |

!!! example
    ```
    ~$ ./run.sh utils.RecoveryVer2 transfers list-guids ...
    ```

### rename

Change the name of a transfer with a specific transferID. Command line options are as follows:

| Required | Option | Parameter _(Default)_ | Description |
|:--------:|:-------|:----------------------|:------------|
|| -id | <a href="#">transferID</a> | Transfer ID |
|| -name | 'Transfer title' | New title to be given to existing transfer |

!!! example
    ```
    ~$ ./run.sh utils.RecoveryVer2 transfers rename ...
    ```

### restart

Restart transfers by their transferIDs.

!!! example
    ```
    ~$ ./run.sh utils.RecoveryVer2 transfers restart _transferID_
    ```

### split

Split transfers by their transferIDs.

| Required | Option | Parameter _(Default)_ | Description |
|:--------:|:-------|:----------------------|:------------|
|| -i, -id | <a href="#">transferID</a> | Source transfer ID |
|| -t, -target | <a href="#">transferID</a> | Target transfer ID |
|| -s, -se | <a href="#">ALICE::NAME::SE</a> | Target storage element |
|| -l, -limit | <integer> | Number of files to transfer |
|| -comment | 'Transfer title' | Name to be passed when creating new transfer |

!!! note
    If limit > 0  then it specifies the number of files to move over to transfer split.
       limit == 0 then __all__ files within the source transfer will be moved into the target transfer.
       limit < 0  then the ratio of source transfer files will be moved. For example: -1 = 50% ; -2 = 33.33% ; -3 = 25%

!!! example
    ```
    ~$ ./run.sh utils.RecoveryVer2 transfers split -t 16750 -l 10 -id 16747
    ```
