# Logging

Log processing and storage for Central Services is managed by [ElasticSearch](https://www.elastic.co/elasticsearch/). 

CERN-managed configuration [(docs)](https://esdocs.web.cern.ch) for <https://es-alicecs.cern.ch> instance is available [here](https://gitlab.cern.ch/it-elasticsearch-project/endpoint-alicecs-settings).

Stored data can be visualised using [Kibana](https://es-alicecs.cern.ch) and [monit-grafana](https://monit-grafana.cern.ch). MonALISA integration soon to come.
