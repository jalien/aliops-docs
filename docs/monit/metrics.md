# [WIP] Metrics Overview

Useful terms and references when looking the tables below:

- **Site Name**: For complete list of available sites refer to [this](http://alimonitor.cern.ch/stats?page=vobox_status) list.
- **Rate Metric**: With an optional \_R suffix added to select metrics returns rate of change from the previously reported value.
- **Job States**: The values for job states follow the submission cycle as shown in the [job state diagram](http://alimonitor.cern.ch/img/jobStatus.png)
- **Transfer State**: Similar to job states, transfer jobs follow an submission cycles with states shown in the [transfer state diagram](http://alimonitor.cern.ch/img/transferStatus.png)
- **Transfer Type**: Available values: db\_insert, db\_lookup\_get, get\_by\_id, get\_by\_lfn\_and\_destination
- **Storage Element**: Full list of Storage elements is available [here](http://alimonitor.cern.ch?632) with types that include but are not limited to SE, SE2, EOS, CTA and TAPE.
- **Central Machines**: For central services node metrics are available for <a href="http://alimonitor.cern.ch/stats?page=machines/machines">these machines</a>
- **Virtual Machines**: For virtual machines used mainly for testing and build, see <a href="http://alimonitor.cern.ch/stats?page=machines%2Fvirtualbox">here</a> for the list.
- **CPU Type**: Available values: guest, idle, iot, iowait, nice, softint, steal, sys, usr.
- **Job Agent State**: Available states: DONE, ERROR\_DIRS, ERROR\_GET\_JDL, ERROR\_HC, ERROR\_IP, ERROR\_JDL, ERROR\_START, INSTALLING\_PKGS, JOB\_STARTED, REQUESTING\_JOB, RUNNING\_JOB
- **Task Queue Access**: Available values: db_lookup, get_match_job, queue_open
- **Task Queue Type**: Available values: ACTIONS\_update, JOBMESSAGES\_insert, db\_lookup, db\_update, get\_jdl, getmasterjob\_stats, getmasterjob\_stats\_time [\_avg,max,min,sum], getmasterjobs\_time [\_cnt,avg,max,min,sum], getsubjobs, getsubjobs\_time[\_cnt,avg,max,min,sum], jobdetails, jobdetails\_time [\_avg,cnt,max,min,sum], matching\_jobs\_summary
- **Query Cache Type**: Available values: abs_hit_percent, abs_hits, abs_miss_percent, abs_misses, abs_total, hit_percent, hits, miss_percent, misses, total
- **Process State**:
  Available values:
  - [D]ead: interruptible sleep
  - [R]unnable: on the running queue
  - [S]leeping
  - [T]raced: stopped
  - [Z]ombie: brains!
  - [W]: no resident pages reserved
  - [X]: dead process 

## Central Services

The following table summarises available monitoring metrics with <a>variables</a> shown in red.

### Catalogue

| Farm    | Cluster | Node              | Parameter  | <a href="../#rate-metric">Rate</a> | Description | Units |
|:--------|:--------|:------------------|:-----------|:----:|:------------|:-----:|
| aliendb1.cern.ch | ALIEN\_alien.catalogue.CatalogueUtils\_Nodes<br>ALIEN\_alien.catalogue.CatalogueUtils\_Nodes\_Summary | <a href="../#central-machines"><db node\></a><br>min, max, sum, med | INDEXTABLE\_lookup | | Number of index table lookups | |
| | | | HIT\_sitequeueupdate | | <mark>Check!</mark> Number of site hits for lookups | |
| | | | p3\_usage | | <mark>Check!</mark> | |
| | | | tempCache\_abs\_hits | &#9745; | Number of cache hits | |
| | ALIEN\_alien.catalogue.GUID\_Nodes<br>ALIEN\_alien.catalogue.GUID\_Nodes\_Summary | <a href="../#central-machines"><db node\></a><br>min, max, sum, med | [GUID,LFN,PFN,LFNREF]\_db_[delete,lookup,insert,update]<br>[GUID,LFN,PFN,LFNREF]\_purged\_pfns | | Statistics on db access | |
| | | | TOTAL\_usedMB | | <mark>Check!</mark> Amount of disk space used by the catalogue? 0.0 | MB |
| | ALIEN\_alien.catalogue.GUIDUtils\_Nodes<br>ALIEN\_alien.catalogue.GUIDUtils\_Nodes\_Summary | <a href="../#central-machines"><db node\></a><br>min, max, sum, med | [GUID,LFN,PFN,LFNREF]\_db\_[delete,lookup,insert,update]<br>[GUID,LFN,PFN,LFNREF]\_purged\_pfns | &#9745; | Statistics on db access | |
| | | | tempCache\_abs\_hits | &#9745; | Number of cache hits | |
| | ALIEN\_alien.catalogue.IndexTableEntry\_Nodes<br>ALIEN\_alien.catalogue.IndexTableEntry\_Nodes\_Summary | <a href="../#central-machines"><db node\></a><br>min, max, sum, med | LFN\_db\_[lookup,find] | &#9745; | Number of LFN lookups | |
| | | | tempCache\_abs\_hits | &#9745; | Number of cache hits | |
| | | | p3\_usage | | <mark>Check!</mark> | |
| | ALIEN\_alien.catalogue.LFN\_Nodes<br>ALIEN\_alien.catalogue.LFN\_Nodes\_Summary | <a href="../#central-machines"><db node\></a><br>min, max, sum, med | LFN\_[delete,list] | | LFN access statistics | |
| | | | p3\_usage | | <mark>Check!</mark> | |
| | ALIEN\_alien.catalogue.LFNUtils\_Nodes<br>ALIEN\_alien.catalogue.LFNUtils\_Nodes\_Summary | <a href="../#central-machines"><db node\></a><br>min, max, sum, med | LFN\_findByMetadata | | <mark>Check!</mark> | |

### Transfers

| Farm    | Cluster | Node              | Parameter  | <a href="../#rate-metric">Rate</a> | Description | Units |
|:--------|:--------|:------------------|:-----------|:----:|:------------|:-----:|
| aliendb1.cern.ch | ALIEN\_alien.io.protocols.TempFileManager\_Nodes<br>ALIEN\_alien.io.protocols.TempFileManager\_Nodes\_Summary | <a><node name\></a><br>min, max, sum, med | <a href="../#job-states"><state\></a>\_jobs | &#9745; | <mark>Check!</mark> Transfer jobs? | |
| | | | temp\_put | | <mark>Check!</mark> | |
| | | | <a><interface\></a>\_[in, out, IN, OUT, COLLS, errs] | | <mark>Check!</mark> | |
| | | | persistent\_put | &#9745; | <mark>Check!</mark> | |
| | | | tempCache\_abs\_hits | &#9745; | Number of cache hits | | 
| | ALIEN\_alien.io.Transfer\_Nodes<br>ALIEN\_alien.io.Transfer\_Nodes\_Summary | <a href="../#central-machines"><central node\></a><br>min, max, sum, med | transfer\_MB | &#9745; | Amount of data being transferred | MB |
| | | | transfer\_MB\_[avg,cnt,max,min,sum] | &#9745; | Summary for data transfers on a specific db node | |
| | | | transfer\_status\_[0-3] | &#9745; | Count on the number of transfers with specific status | |
| | | | transfer\_time\_[avg,cnt,max,min,sum] | &#9745; |  Summary of transfer times | |
| | | | tempCache\_abs\_hits | &#9745; | Number of cache hits | |
| | ALIEN\_alien.io.TransferUtils\_Nodes<br>ALIEN\_alien.io.TransferUtils\_Nodes\_Summary | <a href="../#central-machines"><db node\></a><br>min, max, sum, med | TRANSFERS\_db\_lookup | | DB lookups from transfers | |
| | | | TOTAL\_usedMB | | Amount of disk space used by the catalogue | MB |
| | TransferQueue\_Transfers\_ALICE | <a><job id\></a> | statusID | | Status of the job in the transfer queue | |

### Task Queue

| Farm    | Cluster | Node              | Parameter  | <a href="../#rate-metric">Rate</a> | Description | Units | 
|:--------|:--------|:------------------|:-----------|:----:|:------------|:-----:|
| aliendb1.cern.ch | ALIEN\_alien.taskQueue.JobBroker\_Nodes<br>ALIEN\_alien.taskQueue.JobBroker\_Nodes\_Summary | <a href="../#central-machines"><db node\></a><br>min, max, sum, med | TQ\_[db\_lookup, get\_match\_job, queue\_open] | &#9745; | DB access statistics by the job broker | |
| | | | TOTAL\_usedMB | | Disk space used by the job broker | MB |
| | ALIEN\_alien.taskQueue.JobToken\_Nodes | <a href="../#central-machines"><db node\></a> | jobToken\_db\_update | | Number of token updates | |
| | ALIEN\_alien.taskQueue.TaskQueueUtils\_Nodes<br>ALIEN\_alien.taskQueue.TaskQueueUtils\_Nodes\_Summary | <a href="../#central-machines"><db node\></a><br>min, max, sum, med | QUEUE\_[db\_lookup, get\_match\_job, queue\_open] | | DB lookups for queue | |
| | | | TQ\_[getmasterjobs, getsubjobs] | | Number of lookups for jobs | |
| | | | TOTAL\_usedMB | | Disk space used by the task queue | MB |
| CERN | TaskQueue\_Jobs\_[Alice,ALICE] | <a><vobox node\></a><br>NO\_SITE | job[ID,id] | | IDs for jobs running through the vobox | |
| | | | StatusID | | <mark>Check!</mark> | |

### AliEn

| Farm    | Cluster | Node              | Parameter  | <a href="../#rate-metric">Rate</a> | Description | Units | 
|:--------|:--------|:------------------|:-----------|:----:|:------------|:-----:|
| aliendb3 | ALIEN\_alien.servlets.TextCache\_Nodes\_Summary | [min, max, sum, med] | CLEARPATTERN\_access | &#9745; | | | 2 |
| | | | ms\_to\_answer\_cnt | | Time it takes to respond | ms | 2 |
| | ALIEN\_alien.servlets.TextCache\_Nodes | <a><db node\></a> | SET\_jobbroker | | | 4 |
| | | | whereis\_size | | Size of whereis cache/database? | MB | 4 |
| aliendb1 | ALIEN\_alien.site.JobAgent\_Nodes\_Summary | [min, max, sum, med] | TOTAL\_usedMB | | Disk space used by the job agent | MB | 4 |
| | | | ... More available ... | | | |
| pcalimonitor3 | ALIEN\_alien.user.LDAPHelper\_Nodes | <a><db node\></a> | querycache\_<a href="../#query-cache-type"><type\></a> | &#9745; | Number of misses for query cache | | 7 |
| aliendb1 | ALIEN\_alien.user.LDAPHelper\_Nodes\_Summary | [min, max, sum, med] | TOTAL\_usedMB | | Disk space used by the LDAP Helper | MB | 4 |
| | | | ... More available ... | | | |
| CERN | CERN\_ApiService | \_TOTALS\_ | n\_user | | Number of current users | | 1 |
| | | <a><pcapiserv node:port\></a> | n\_sessions | | Number of current sessions | | 1 |
| | CERN\_DAQ | <a><node\></a> | count | &#9745; | | | 1 |
| | | | delay\_avg | | | | 2 |
| | CERN\_DAQ\_Nodes | <a><node\></a> | load1 | | System load average for past 1 minute (uptime) | | 1 |
| | | | cpu\_MHZ | | Frequency of the CPU | MHZ | 1 |
| | CERN\_DAQ\_Nodes\_Summary | [min, max, sum, med] | sockets\_tcp\_SYN\_RECV | | Number of sockets with SYN\_RECV\_status | | 3 |
| | | | cpu\_idle | | Percentage of current CPU idleness | | 3 |
| | CERN\_IS | <a><https site:port\></a> | cpu\_time | | Example: https://aliendb8.cern.ch | | 3 |
| | | | rss, virtualmem | | Amount of resident and virtual memory | B | 2 |
| | CERN\_JobManager | <a><https site\></a> | cpu\_time | | Example" https://aliendb8.cern.ch | | 6 |
| | | | rss, virtualmem | | Amount of resident and virtual memory | B | 2 |
| | CERN\_Optimizer\_Packages | <a><node\></a> | pid | | ProcessID of package optimizer | | 2 |

### Machines

| Farm    | Cluster | Node              | Parameter  | <a href="../#rate-metric">Rate</a> | Description | Units | 
|:--------|:--------|:------------------|:-----------|:----:|:------------|:-----:|
| <a href="../#central-machines"><central node\></a> | Machine | apiserv | Status | | Status of the API service: 0.0=OK, 1.0=FAIL | |
| | | CCISS[1-7] | Raid | | RAID level for HP Smart Array CCISS driver | |
| | | disk | Status | | Status of the disk: 0.0=OK, 1.0=FAIL | | 
| | | ipmi | 01-[Inlet,Front] Ambient, 02-CPU 1, Fan 1 | | Temperature sensor readings at key locations | <sup>o</sup>C |
| | | | Power Meter | | Node power usage on out | Watts |
| | | kernel | uptime | | Uptime of the machine | s |
| | | md[0,123] | Size | | <mark>Check!</mark> RAID array size | Bytes |
| | | memory | total | | Amount of RAM on the machine | Bytes | 
| | | mysql[,2,3] | seconds\_behind\_master | | Number of seconds behind master node | s | 
| | | | Status | | MySQL service monitoring <a href="https://dev.mysql.com/doc/refman/8.0/en/monitoring-innodb-cluster.html">reporting level</a> | |
| | | sensors | Core 0 | | Temperature sensor readout near Core 0 | <sup>o</sup>C | 
| | | System | Semaphores | | Number of semaphores currently run by OS: [Mostly 0.0] | |
| aliendb1.cern.ch | VMMonitor | <a href="../#virtual-machines"><vm node\></a> | block\_in | &#9745; | File blocks being read in by the OS | |
| | | | bogomips | | CPU speed measured by the kernel | | 
| | | | Disk\_Status | | Status of the disks: 0.0=OK | | 
| aliendb1.cern.ch | ALICE::CERN::CENTRALSERVICES\_SysDiskDF\_Nodes | <a href="../#central-machines"><central node\></a> | p0\_sizeMB | | Amount of disk space available (df) | MB | 
| | ALICE::CERN::CENTRALSERVICES\_SysDiskIO\_Nodes | '' | [loop<a><0-4\></a>, md12<a><3-6\></a>, sda, cciss!c0d0]\_ReadMBps | | Speed of reading from disk device | MB/s |
| | | | TOTAL\_devices | | Total number of disk devices | |
| | ALICE::CERN::CENTRALSERVICES\_SysNetIO\_Nodes | '' | [eth0-3,p2p2]\_IN | | Incoming speeds on network interfaces | Gb/s |
| | | | rmem\_max | | <mark>Check!</mark> Receive buffer max setting (sysctl.conf): 8192.0 | B |
| | | | sockets\_tcp[\_LISTEN] | | Number of open TCP socket connections open | |
| | ALICE::CERN::CENTRALSERVICES\_SysStat\_Nodes | '' | cpu\_MHz | | Frequency of the CPU | MHz |
| | | | CPU\_usr | | CPU load on the host | % |
| | | | Load1 | | System load average for past 1 minute | | 
| | | | processes\_<a href="../#process-state"><state\></a> | | Number of processes in specific state | |
| | | | total\_mem | | Total amount of RAM memory | MB |
| aliendb1.cern.ch<br>LBL\_HPCS | ALIEN\_Self\_CE\_Nodes | <a><vobox node\></a> | job\_rss | | Resident memory of CE job | MB |
| | | | jvm\_pid | | Process ID of the JVM process | |  
| <a href="../#site-name"><site\></a> | ALIEN\_Self\_ChecksumFileCrawler\_Nodes\_Summary | max, med, min, sum | <a><interface\></a>\_[out,errs,COLLS] | | Statistics on network interfaces | |
| | | | tempCache\_abs\_hits | &#9745; | Cache hits | | 
| aliendb[1-3].cern.ch | ALIEN\_Self\_Crawler\_Nodes\_Summary | | | |
| aliendb[1-3].cern.ch | ALIEN\_Self\_CrawlingPrepare\_Nodes\_Summary | | | |
| aliendb[1-3].cern.ch | ALIEN\_Self\_Cron\_Nodes\_Summary | | | |
| aliendb[1-3].cern.ch | ALIEN\_Self\_FaH\_Nodes\_Summary | | | |
| aliendb[1-3].cern.ch | ALIEN\_Self\_IterationPrepare\_Nodes\_Summary | | | | 
| aliendb[1-3].cern.ch | ALIEN\_Self\_JBox\_Nodes | | | |
| aliendb[1-3].cern.ch | ALIEN\_Self\_JBox\_Nodes\_Summary | | | |
| aliendb[1-3].cern.ch | ALIEN\_Self\_JCentral\_Nodes | | | |
| aliendb[1-3].cern.ch | ALIEN\_Self\_JCentral\_Nodes\_Summary | | | |
| aliendb[1-3].cern.ch | ALIEN\_Self\_JobAgent\_Nodes\_Summary | | | |
| aliendb[1-3].cern.ch | ALIEN\_Self\_JobWrapper\_Nodes | | | |
| aliendb[1-3].cern.ch | ALIEN\_Self\_JobWrapper\_Nodes\_Summary | | | |
| aliendb[1-3].cern.ch<br>alihyperloop.cern.ch<br>CERN, LBL\_HPCS | ALIEN\_Self\_Nodes | <a href="../#central-machines"><central node\></a> | cpu\_[time,usage] | | <mark>Check!</mark>CPU time and usage | |
| | | | disk\_[free,total,usage,used] | | <mark>Check!</mark>Disk use (du)? | | 
| | | | user.[dir,name] | | <mark>Check!</mark> | | 
| | | | processes | | <mark>Check!</mark> Number of running processes | |
| | | | workdir\_size | | <mark>Check!</mark> | |
| | | | java.version | | <mark>Check!</mark> Version of Java running on the node | |
| | | | java.vm.vendor | | <mark>Check!</mark> | |
| | | | job\_[rss,virtualmem] | | RAM and virtual memoery used | B |
| | | | mem\_usage | | <mark>Check!</mark> | B | 
| | | | open\_files | |  <mark>Check!</mark> Number of open files | | 
| | | | run\_time | | <mark>Check!</mark> AliEN run time | s |
| | | | jvm\_pid | | Process ID of the JVM process on host | |
| | | | jvm\_uptime | | <mark>Check!</mark> JVM uptime | |
| | | | jvm\_virtualmem | | <mark>Check!</mark> Virtual memory allocated to the JVM process | |
| | | | jvm\_available\_[memory,processes] | | <mark>Check!</mark> | | 
| | | | jvm\_[used,free,max,total]\_memory  | | <mark>Check!</mark> | |
| aliendb[1-3].cern.ch<br><a href="../#site-name"><site\></a> | ALIEN\_Self\_Nodes\_Summary | [min, max, sum, med] | CPU\_sys | | <mark>Check!</mark> | |
| | | | <a><interface\></a>\_[out,errs,COLLS,IN] | | Statistics on network interfaces | |
| | | | HIT\_sitequeueupdate | | Hits on queue updates? | |
| | | | object\_cache\_size | | <mark>Check!</mark> | |
| | | | tempCache\_abs\_hits | &#9745; | Cache hits | |
| aliendb[1-3].cern.ch | ALIEN\_Self\_OutputMerger\_Nodes\_Summary | | | |
| aliendb[1-3].cern.ch | ALIEN\_Self\_SEFileCrawler\_Nodes\_Summary | | | |
| aliendb[1-3].cern.ch | ALIEN\_Self\_SubmitJobs\_Nodes\_Summary | | | |
| aliendb[1,3]<br>pcalimonitor3 | ALIEN\_System\_Nodes | <a><node\></a> | bogomips | | CPU speed measured by the kernel | bmps | 
| | | | CPU\_usr | | CPU use in user space i.e. load on the host | % | 
| | | | eth0\_IN | | Incoming bandwidth on eth0 interface | Gb/s | 
| | | | Load[1,5,15] | | System load for the past 1, 5 and 15 mins (uptime) | | 
| | | | p0\_sizeMB | | Amount of disk space available (df) | MB | 
| | | | total\_mem | | Total amount of RAM memory | MB | 
| aliendb1 | ALIEN\_System\_Nodes\_Summary | [min, max, sum, med] | ... More available ... | | | 
| <a href="../#site-name"><site\></a> | AliEnServicesStatus, AliEnServicesLogs | Authen | 0.0=OK, 1.0=FAIL | | Per site status report for specific service | | 
| | | Broker | | | | | |
| | | CatalogueOptimizer | | | | | |
| | | CatalogueOptimizer_[Expired,Packages,Trigger] | | | | | |
| | | [CE, SE, IS, CMreport] | | | | | |
| | | JobBroker | | | | | |
| | | JobInfoManager | | | | | |
| | | JobOptimizer | | | | | |
| | | JobOptimizer\_[Expired,Inserting,Killed,Merging]<br>JobOptimizer\_[Saved,Splitting,Zombies,Charge,HeartBeat]<br>JobOptimizer\_[Hosts,MonALISA,Priority,Resubmit] | | | | | |
| | | MessageMaster | | | | | |
| | | MonaLisa | | | | | |
| | | Monitor, Monitor\_ProcInfo | | | | | |
| | | PackMan, PackManMaster | | | | | |
| | | SCRIPTRESULT | | | | | |
| | | SEManager | | | | | |
| | | ServerSE\_xrootd.default | | | | | |
| | | TransferManager | | | | | |
| <a href="../#site-name"><site\></a> | AliEnTestsStatus | alien<br>ldap<br>alien\_proxy<br>alien\_version<br>SCRIPTRESULT | 0.0=OK. 1.0=FAIL | | Per site service tests | |
| | | | timeleft | | | | |
| | | | Message | | | | |  
| <a href="../#central-machines"><central node\></a> | UPS | localhost | ups\_[LINEV, MBATTCHG] | | UPS reported parameters | |

### Network

| Farm    | Cluster | Node              | Parameter  | <a href="../#rate-metric">Rate</a> | Description | Units | 
|:--------|:--------|:------------------|:-----------|:----:|:------------|:-----:|
| aliendb9.cern.ch<br>ISS\_LCG | WAN | 192.168.1.<a><n\></a> |  <a href="../#central-machines">node</a>\_IN, Uplink-<a href="../#central-machines">node</a>\_IN, Uplink\_IN| | <mark>Check!</mark>Incoming bandwidth | Mb/s |
| | WAN\_Stats | 192.168.1.<a><n\></a> | Uplink[-aliswitch6,8]\_AdminStatus| | <mark>Check!</mark>Admin set status: 1.0=OK? | |
| | | | Uplink[-aliswitch6,8]\_SPEED | | Speed measured at the switch | Mb/s |

## Storage Metrics

The following table summarises available monitoring metrics with <a>variables</a> shown in red.

| Farm   | Cluster | Node             | Parameter  | <a href="../#rate-metric">Rate</a> | Description | Units | Frequency (min<sup>-1</sup>) |
|:-------|:--------|:-----------------|:-----------|:------:|:------------|:-----:|:----------------------:|
| CERN   | ALICE\_CS\_Transfers\_Summary | <a href="../#storage-element"><SE\></a><br>GLOBAL<br>\_TOTALS\_ | <a href="../#transfer-state"><state\></a>\_transfers | &#9745; | Number of transfers currently in specific state | | 12 |
| | ALICE\_SEs\_Transfers\_Summary | <a href="../#storage-element"><SE\></a><br>\_TOTALS\_ | transf\_mbytes | &#9745; | Amount of data transferred | MB | 11 |
| | | | transf\_speed\_mbs | | Speed of transfers | MB/s | 11 |
| <a href="../#site-name"><site\></a> | FDT\_Server\* | <a href="../#site-name"><site\></a> | NET\_IN\_Mb | | Amount transferred inside the site | MB | 322 |
| | | fdtspeed\_<a><pid\></a> | TransferredMBytes | | Amount of data transferred by clients | MB | 4 |
| | | | <a>1.0=Stopped/Finished</a> | | Current transfer status | | 1 |
| | FDT\_Transfers | fdtspeed\_<a><pid\></a> | TransferredMBytes | | Amount of data transferred by clients | MB | 12 |
| | | | period | | Value seems to be consistent: 60.0 | | 4 |
| | | | ExitCode | | Transfer process exit code. Observed value: 99.0 | | 2 |
| | [Site\_]SE\_AliEnTraffic\_Summary | <a href="../#site-name"><site\></a>-<a href="../#storage-element"><type\></a> | [read,write]\_mbytes | &#9745; | Amount of data read/written at site storage | MB | 176 |
| | | | write\_[successes,failures] | &#9745; | Number of write successes and failures | | 52 |
| | SE\_Traffic\_Summary | <a href="../#storage-element"><SE\></a> | transf\_[wr,rd]\_files | &#9745; | Total number of files transfer for read/write | | 316 |
| | Site\_Traffic\_Summary | [Incoming,Outgoing]\_<a href="../#site-name"><site\></a></br><a href="../#site-name"><site\></a>-<a href="../#site-name"><site\></a> | transf\_mbytes | &#9745; | Total of data transferred | MB | 1,811 |
| | | | transf\_[rd,wr]\_files | &#9745; | Total number of files transferred for read/write | | |
| <a href="../#site-name"><site\></a> | SE\_[READ,WRITE]\_<a href="../#storage-element"><SE\></a> | JOB\_<a><job id\></a> | time | | Time of the read/write transfers | s | 4,286 |
| <a href="../#site-name"><site\></a><br>aliendb<a><n\></a> | XrdServers | <a><node\></a> | srv\_conn\_clients | | Number of connected clients | | 1,229 |
| | XrdServers\_Aggregate | client\_C\_class | <a><source IP\></a>\_[IN,OUT] | | Incoming speed of transfers | MB/s | 1,063 |
| | | lan\_wan | [LAN,WAN]\_[IN,OUT] | | Internal site bandwidth | MB/s | 38 |
| | | site | <a href="../#site-name"><source site\></a>\_[IN,OUT] | | Inter-site bandwidth | MB/s | 38 |
| | | sum | TOTAL\_[IN,OUT] | | Sum bandwidth incoming to site | MB/s | 38 |
| | XrdServers\_Summary | <a><node\></a><br>\_TOTALS\_ | srv\_[rd,wr]\_mbytes | &#9745; | Amount of reads and write | MB | 499 |
| | \*\_xrootd\_ApMon\_Info | <a><node\></a> | [27 parameters] | | XRootd documentation available ... | | 5,168 |
| | <a href="../#storage-element"><SE\></a>\_xrootd\_Nodes | <a><node\></a> | blocks\_in | &#9745; | File blocks being read by the OS? | | 32 |
| | | | bogomips | | CPU speed measured by the kernel | bmps | 4 |
| | | | cpu\_MHz | | Frequency of the CPU | MHz | 3 |
| | | | CPU\_usr | | CPU use in user space i.e. CPU load on the host | % | 21 |
| | | | [em1,eht0]\_IN | | Incoming speeds for the interfaces | Gb/s | 5 |
| | | | lm\_[Core0,CPUCore1,in0,Physicalid0]\_value<br>lm\_[power1,temp1,VcoreA, VCore]\_value | | Power (units?) and core/sensor temp values | C | 3 |
| | | | load1, Load1 | | System load average for past 1 minute | | 21 |
| | | | <a><device\></a>\_ReadMBps | | Speed of reading from the device | MB/s | 38 |
| | | | rmem\_max | | Receive buffer max setting (sysctl.conf) | B? | 1 |
| | | | sockets\_tcp | | Number of TCP sockets open | | 13 |
| | | | TOTAL\_devices | | Total number of devices | | 10 |
| | | | total\_mem | | Total amount of memory | MB | 3 |
| | \*\_xrootd\_Nodes\_Summary | max, med, min, sum | cpu\_idle | | Percentage of CPU idleness | % | 6 |
| | | | count | | Number of xrootd nodes | | |
| | | | Load[1,5,15] | | Average load over the past 1,5,15 mins | | |
| | | | mem\_actualfree | | Amount of disk space free | MB | 1 |
| | | | <a href="../#job-states"><state\></a>\_jobs | | Number of jobs in specific state | | 88 |
| | | | sockets\_tcp\_SYN\_RECV | | Number of sockets with SYN\_RECV status | | 10 |
| | | | TOTAL\_usedMB | | Total amount of disk used | MB | 4 |
| | \*\_xrootd\_SysInfo | <a><node\></a> | blocks\_in | &#9745; | File blocks being read in by the kernel | | 95 |
| | | | processes | | Number of processes on the system | | 72 |
| | [<a href="../#storage-element"><SE\></a>\_]xrootd\_Services | <a><node\></a> | [cpu,run]\_time | | CPU and wall time running xrootd service | s | 185 |
| | | | rss, virtualmem | | Amount of memory used by XRootD services | B | 11 |
| | | | space\_[free,total,largestfreechunk] | | Amount of space available | MB | 212 |

## Site Metrics

The following table summarises available monitoring metrics with <a>variables</a> shown in red.

| Farm    | Cluster | Node              | Parameter  | <a href="../#rate-metric">Rate</a> |  Description | Units | Frequency (min<sup>-1</sup>) |
|:--------|:--------|:------------------|:-----------|:----:|:------------|:-----:|:----------------------:|
| <a href="../#site-name"><site\></a> | \*\_CE\_\* | <a><vobox node\></a> | cpu_[time,usage] | | vobox node time and usage |  | 85 |
| | | | jobAgents_[queued,running,slots] | | Count for the job agents in specific state/count | | 84 |
| | | | rss, virtualmem | | Amount of memory used by the CE | B | 4 |
| | | | mem\_usage | | CE memory usage | | |
| | | | open\_files | | Number of open files | | |
| | | | page\_faults\_[max,min] | | Number of OS page faults | | |
| | | | run\_time | | Run time of CE | | | 
| <a href="../#site-name"><site\></a> | \*\_JobAgent | <a><node:port\></a> | cpu\_ksi2k | | Kilo SpecINT2000 Benchmark unit | ksi2k | 6,997 |
| | | | cpu\_time | | System running time of job agent | s | 876 |
| | | | numjobs | | Number of jobs passing through the job agent | | 2 |
| | | | rss, virtualmem | | Amount of resident and virtual memory used by the job agent | B | 18,178 |
| | | | job\_id, JOB\_ID | | Job IDs launched through relevant queue manager | | 12,000 |
| | | | LSB\_BATCH\_JID<br>LSB\_JOBID<br>SLURM\_JOBID<br>SLURM\_JOB\_ID | | | | |
| <a href="../#site-name"><site\></a> | \*\_JobAgent\_Summary | histograms | numjobs\_[1-16] | | + numjobs\_ja\_end\_R. Used for graph building | | 77 |
| | | status | <a href="../#job-agent-state"><state\></a>\_ja | &#9745; | Error status: 7.0, 10.0, 11.0, 12.0, -3.0, -10.0 | 79 |
| | | ce | jobAgents\_[queued,running,slots] | | Number of job agents in specific state | | 72 |
| | | avg | ttl | | Average lifetime of a job agent | s | 54 |
| <a href="../#site-name"><site\></a>  | ABPing  | <a><target host/ip\><a>  | Jitter | | Measure of latency variation over time | ms | 571 |
| | | | PacketLoss | | Percentage of packets lost | % | 555 |
| | | | RTT        | | Round trip time between site - vobox node | ms | 555 |
| | | | RTime      | | No idea |  | 1,208 |
| | IPs | | ... More available ... | | | | |
| <a href="../#site-name"><site\></a> | Link\_\* (NERSC-\>Net) | default | inReqCount | | Internal request count? | | 1 |
| <a href="../#site-name"><site\></a><br><a><central node\></a> | Tracepath | <a><target host\></a> | <a><[0-x]:ip\></a> | | Tracepath from [0]:source to target | | 44 |
| | | | status | | | |
| <a href="../#site-name"><site\></a><br><a><central node\></a> | Master | <a><hostname, localhost\></a> | acpi\_[THM0,TZ01]\_value | | Thermal zone values | C | 8 |
| | | | blocks\_in | &#9745; | File blocks being used by the OS? | | 241 |
| | | | bogomips | | CPU speed measurement made by the kernel | bmps | 11 |
| | | | bond0\_IN | | Incoming speed for virtual link on bond0 interface | Gb/s | 4 |
| | | | CACHE\_DIR\_[free,total,usage,used] | | Statistics for the cache folder | % | 2 | 
| | | | CE.db\_MESSAGES\_du\_MB | | CE messages stored in the database. Example: aliendb[2,5] | MB | 2 |
| | | | CPU\_user | | CPU use in user space i.e. CPU load on the host | % | 108 |
| | | | em[1-3]\_IN, eth0\_IN | | Incoming speeds for embedded interfaces | Gb/s | 32 |
| | | | lm\_[Core0,CPUCore1,ln0,Physicald0]\_value | | Power units and core/sensor temperature values | C | 56 |
| | | | lm\_[power1,temp1,VcoreA,VCore]\_value | | | | | 
| | | | Load1 | | System load average for past 1 minute | | 105 |
| | | | LOG\_DIR\_[free,total,usage,user] | | Statistics for log dir folder | % | 208 |
| | | | processes | | Number of precesses on the system | | 91 |
| | | | TMP\_DIR\_[free,total,usage,user] | | Statistics for the tmp folder | % | 7 |
| | | | vboxnet0\_IN | | Message from alienvm3. Observed value: 0.0 | | 4 |
| <a href="../#site-name"><site\></a> | Monitoring\_Sensors | monitor.<a><node\></a> | lm\_[Core0,CPUCore1,in0,Physicald0]\_value<br>lm\_[power1,temp1,VcoreA,VCore]\_value | | Reported temperature for the node? | C | 1 |
| | Monitoring\_SysDiskDF | monitor.<a><node\></a> | p0\_sizeMB | | Amount of disk space available (df) | MB | 1 |
| | Monitoring\_SysDiskIO | monitor.<a><node\></a> | <a><device\></a>\_ReadMBps | | Speed of reading from the device | MB/s | 24 |
| | | | TOTAL\_devices | | Total number of mounted devices | | 4 |
| | Monitoring\_SysNetIO | monitor.<a><node\></a> | eth0\_IN | | Incomding bandwidth for ethernet interfaces | MB/s | 4 | 
| | | | rmem\_max | | Receive buffer max setting (sysctl.conf) | B? | 1 |
| | | | sockets\_tcp | | Number of open TCP sockets | | 2 |
| | Monitoring\_SysStat | monitor.<a><node\></a> | cpu\_MHz | | Frequency of the CPU | MHz | 1 |
| | | | CPU\_user | | CPU load on the host? | % | 4 |
| | | | Load1 | | System load average for past 1 minute (uptime) | % | 4 |
| | | | processes\_<a href="../#process-state"><state\></a> | | Number of processes in specific state | | 2 |
| | | | total\_mem | | Total amount of memory | MB | 1 |
| <a href="../#site-name"><site\></a> | \*\_Nodes | <a><node\></a> | bogomips | | Speed measured by the kernel | bmps | 8 |
| | | | blocks\_in | &#9745; | File blocks being used by the OS | | 8,251 |
| | | | <a href="../#job-state"><state\></a>\_jobs | &#9745; | Number of jobs in specific state | 19 |
| | | | processes | | Number of processes on the system | | 26,558 |
| <a href="../#site-name"><site\></a> | Nodes\_Summary | max, med, min, sum | CLEARPATTERN\_access | &#9745; | | | 2 |
| | | | cpu\_idle | | Percentage of CPU idleness | % | 19 |
| | | | mem\_actualfree | | Amount of disk space free | MB | 3 |
| | | | ms\_to\_answer\_cnt | | ms | 2 |
| | | | count | | Number of worker nodes | | |
| | | | processes | | Number of processes | | 234 |
| | | | <a href="../#job-state"><state\></a>\_jobs | | Number of jobs in specific state | | 392 |
| | | | sockets\_tcp\_[,SYN\_RECV] | | Number of open or in specific state sockets | | 20 |
| | | | TOTAL\_usedMB | | Total amount of disk used | MB | 4 |
| <a href="../#site-name"><site\></a><br><a><central node\></a> | MonLisa | localhost | ApMon\_8884\_lostDatagrams | | Number of datagrams lost from site to Monalisa | | 96 |
| | | | CPU\_usr | | CPU use in user space i.e. CPU load on the host | % | 224 |
| | | | CurrentParamNo | | Parameters available for monitoring | | 226 |
| | | | em[1-4]\_IN<br>eth[0-2]\_IN | | Incoming speeds for embedded and eth0 interfaces | Gb/s | 181 |
| | | | Load1 | | System load average for past 1 minute (uptime) | | 225 |
| | | | Max\_Memory | | Maximum RAM memory used by MonALISA? | MB | 228 |
| | | | memstore\_size | | Current in memory store size before it is flushed | MB | 211 |
| | | | MLCPUTime | | MonALISA process relative CPU time (top) | % | 228 |
| | | | MLH\_[RCount,TopoSerCount] | | | | 228 |
| | | | PMS\_Queue\_Sizee | | | | 228 |
| | | | PW\_[NewPItemsChanged,NewSIDChanged] | | | | 228 |
| | | | TCW\_[PoolSize,TotalAdded] | | | | 228 |
| | | | TotalLocalProcesses | | Total local prcesses on the host | | 228 |
| | | | vboxnet0\_IN, veth<a><id\></a>\_IN | | Incoming bandwidth on virtual interfaces | Gb/s | 3 |
| | MonaLisa\_DiskDF | localhost | p0\_sizeMB | | Amount of disk space available (df) | MB | 44 |
| | MonaLisa\_LocalSysMon | localhost | <a><device\></a>\_ReadMBps | | Speed of reading from device | | 248 |
| | | | TOTAL\_devices | | Total number of mounted devices? | | 248 |
| | MonaLisa\_MemInfo | localhost | total\_mem | | Total amount of RAM available | MB | 185 |
| | MonaLisa\_NetworkConfiguration | localhost | rmem\_max | | Receive buffer max setting(sysctl.conf) | B? | 14 | 
| | MonaLisa\_SysInfo | localhost | bogomips | | CPU speed measurement made by the kernel | bmps | 16 |
| | | | cpu\_MHz | | Frequency of the CPU. Reported by aliendb06e | MHz | 1 |
| | MonaLisa\_ThPStat | Monitor.[Executor,modules,Network.Executor,Store.Executor] | ActiveCount | | Number of these processes running? | | 246 |
| | | util.process | ActiveCount | | Utility processes running per site? | | 246 |
| <a href="../#site-name"><site\></a> | \*\_cmsd\_Services | <a><node\></a> | [cpu,run]\_time | | System and walltime of CMSD | s | 119 |
| | | | rss, virtualmem | | Amount of resident and virtual memory taken by CMSD | B | 48 |
| <a href="../#site-name"><site\></a> | LCGServiceStatus | Delegated\_proxy | Status: 0.0=OK, 1.0=FAIL | | Status for specific LCG service at the size | | 77 |
| | | Proxy\_[of\_the\_machine,Renewal,Server] | | | | | 77 |
| | | gsissh | | | | | 77 |
| | | Software\_area | | | | | 77 |
| | | SCRIPTRESULT | | | | | 77 |

## Job Metrics

The following table summarises available monitoring metrics with <a>variables</a> shown in red.

| Farm   | Cluster | Node             | Parameter  | <a href="../#rate-metric">Rate</a> | Description | Units | Frequency (min<sup>-1</sup>) |
|:-------|:--------|:-----------------|:-----------|:----:|:------------|:-----:|:----------------------:|
| CERN   | ALICE\_AllJobs | States | <a href="../#job-state"><state\></a> | | Number of jobs in specific state | | 1 |
| | ALICE\_CS\_Jobs\_Summary | jobs | <a href="../#job-state"><state\></a>\_jobs | &#9745; | Number of jobs in specific job state | | 1 |
| | ALICE\_Sites\_Jobs\_Summary | <a href="../#site-name"><site\></a> | <a href="../#job-state"><state\></a>\_jobs | &#9745; | Number of jobs in specific state | | 77 |
| | ALICE\_Users\_StatusTransit\_Summary | <a href="../#site-name"><site\></a> | <a href="../#job-state"><state\></a>\_avg || Average number of jobs| | 59 |
| | | | <a href="../#job-state"><state\></a>\_cnt | | Count for number of jobs | | 59 |
| | ALICE\_Users\_Jobs\_Summary | <a><user\></a> | <a href="../#job-state"><state\></a>\_jobs | | Number of jobs in specific state | | 28 |
| | ALICE\_Users\_StatusTransit\_Summary | <a><user\></a> | <a href="../#job-state"><state\></a>\_avg | | Average number of jobs in specific state | | 7 |
| | | | <a href="../#job-state"><state\></a>\_cnt | | Number of jobs in specific status | | 22 |
| <a href="../#site-name"><site\></a> | Site\_UserJobs\_Summary | <a><user\></a> | disk\_usage | | Percent of disk used by the user | % | 382 |
| | | | count | | Number of jobs running at the site | | |
| | | | cpu\_[ksi2k,usage,time] | &#9745; | Usage and timing of the jobs | | 117 |
| | | | host\_pid | | | | |
| | | | masterjob\_id | | | | 
| | | | mem\_usage | | | | |
| | | | open\_file | | Number of files opened | | 2 |
| | | | page\_faults\_[min,max] | | Statistics on the number of page faults | | |
| | | | run\_[ksi2k,time] | &#9745; | Run time of the user jobs | | |
| | | | transf\_[moved,read,written]\_files | &#9745; | Number of files read, moved and written | | |
| | | | trnasf\_[moved,written]\_mbytes | &#9745; | Amount of data moved and written | MB | 2 |
| | | | transf\_speed | | Speed of transfers | MB/s | 1 |
| | | | rss, virtualmem | | Resident and virtual memory used | B | 21 |
| | | | workdir\_size | | Size of the working directory | MB? | |
| <a href="../#site-name"><site\></a> | \*\_Jobs | <a><job id\></a> | cpu\_ksi2k | | Kilo SpecINT2000 Benchmark unit | ksi2k | 9,342 |
| | | | cpu\_time | | System time the job has spent running | s | 1,171 |
| | | | rss, virtualmem | | Amount of resident and virtual memory used by the job | MB | 5,642 |
| | | | masterjob\_id | | Id of the Master Job | | 10,547 |
| | | | transf\_speed | | Transfer speed for files? Observed: 0.0 | MB/s | 47 |
| | | | status: 7.0, 10.0, 11.0, 12.0, -3.0, -10.0 | | Current job status code? | | 121 |
| <a href="../#site-name"><site\></a> | Jobs\_Memory\_Offenders | top[rss,virtualmem]\_user\_[1-3] | rss, virtualmem | | Amount of resident and virtualmem used by the top users | B | 106 |
| <a href="../#site-name"><site\></a> | Jobs\_Summary | top[rss,virtualmem]\_[1-3] | disk\_usage | | Amount of disk space being used by top users | B | 835 |
| | | | transf\_read\_files | | Number of files transferred for reading? | |
| | | | transf\_speed | | Transfer speed of files. Observed value: 0.0 | MB/s | |
| | | [sum, med, min, max] | disk\_usage | | Amount of disk space used overall | % | 853 |
| | | | transf\_read\_files | | Number of files transferred for reading? | |
| | | jobs<br>\_TOTALS\_ | <a href="../#job-states"><state\></a>\_jobs | | Number of jobs in specific state | | 315 | 
| | | <a href="../#site-name"><site\></a> | <a href="../#job-states"><state\></a>\_jobs | | For specific site number of jobs in specific state | | 315 |
| | | <a><user\></a> | <a href="../#job-states"><state\></a>\_jobs | | For specific user number of jobs in specific state | | 315 |
| <a href="../#site-name"><site\></a> | Job\_XRD\_Transfers | <a><job id\></a> | total\_size | | Total amount of XRootd data transferred per job | MB | 7,626 |
